import React from 'react';
import PropTypes from 'prop-types'
import './FourColGrid.css';

const FourColGrid = ({ header, loading, children }) => {
    // ES6 destructuring the state
    // El props children no se envia textualmente desde la fuente(Home.js) pero se debe poner para que quede ES6
    const renderElements = () => {
        const gridElements = children.map( (element, i) => {
            return(
                <div key={i} className="rmdb-grid-element">
                    {element}
                </div>
            );
        });
        return gridElements;
    }

    return(
        <div className="rmdb-grid">
            {header && !loading ? <h1>{header}</h1> : null}
            <div className="rmdb-grid-content">
                {renderElements()}
            </div>
        </div>
    );
}

FourColGrid.propTypes = {
    header: PropTypes.string,
    loading: PropTypes.bool
}

export default FourColGrid;